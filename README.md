This project is used to build [node-mapnik](https://github.com/mapnik/node-mapnik)

It has no other purpose. So it offers the built images for dedicated versions.

All credits go to the maintainers of the node-mapnik project for their great work.

Each image tag belongs to one branch in this repo. So master branch is only used for documentation.

| branch | image (to use in a Dockerfile) |
| ---  |  ---  |
|  [3.0.x](https://gitlab.com/geo-bl-ch/docker/node-mapnik/-/tree/3.0.x)  |  `registry.gitlab.com/geo-bl-ch/docker/node-mapnik:3.0.x`  |

Please refer to the [registry of this project](https://gitlab.com/geo-bl-ch/docker/node-mapnik/container_registry/) to see images in browsable view:


Please see dedicatd branch for details like used libraries.